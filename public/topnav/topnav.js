function changeActive(id, div){
    let active = document.getElementById(div).getElementsByClassName("active")[0];
    let newActive = document.getElementById(id);

    active.classList.remove("active");
    newActive.classList.add("active");
}

let topNavC = document.getElementById("topNav").children;

for (let i = 0; i < topNavC.length; i++) {
    let elem = topNavC[i];
    elem.addEventListener("click", function() {
                                                    changeActive(elem.id, "topNav");
                                                });
}